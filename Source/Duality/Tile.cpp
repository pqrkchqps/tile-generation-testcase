// Fill out your copyright notice in the Description page of Project Settings.

#include "Tile.h"
#include "RuntimeMeshLibrary.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RuntimeMesh = CreateDefaultSubobject<URuntimeMeshComponent>(TEXT("Runtime Mesh"));
	RootComponent = RuntimeMesh;
}

void ATile::SetupTile(int32 halfWidth, float cellSize, float height)
{
	HalfWidth = halfWidth;
	CellSize = cellSize;
	Height = height;
}
void ATile::SetHeightMap(TMap<int32, FVector> heightMap) {
	HeightMap = heightMap;
}

void ATile::GetXYIndexesAroundArrayIndex(int32 Index, int32 *prevX, int32 *prevY, int32 *nextX, int32 *nextY) {
	int32 rowLength = HalfWidth * 2;

	int32 maxIndex = rowLength*rowLength - 1;

	*nextY = Index + rowLength;
	if (*nextY > maxIndex) {
		*nextY = Index;
	}
	*prevY = Index - rowLength;
	if (*prevY < 0) {
		*prevY = Index;
	}
	*nextX = Index + 1;
	if ((*nextX % rowLength) == 0) {
		*nextX = Index;
	}
	*prevX = Index - 1;
	if ((*prevX % rowLength) == (rowLength - 1) || *prevX == -1) {
		*prevX = Index;
	}
}

void ATile::GetDirectionVectorsAroundIndex(int32 Index, int32 prevX, int32 prevY, int32 nextX, int32 nextY, FVector *up, FVector *down, FVector *left, FVector *right) {
	*up = HeightMap[nextY] - HeightMap[Index];
	*down = HeightMap[prevY] - HeightMap[Index];
	*left = HeightMap[nextX] - HeightMap[Index];
	*right = HeightMap[prevX] - HeightMap[Index];
}

FVector ATile::GetNormalsForEachSectionAroundIndex(FVector up, FVector down, FVector left, FVector right) {
	FVector n1, n2, n3, n4;
	n1 = FVector::CrossProduct(left, up);
	n2 = FVector::CrossProduct(up, right);
	n3 = FVector::CrossProduct(right, down);
	n4 = FVector::CrossProduct(down, left);
	return n1 + n2 + n3 + n4;
}
FVector ATile::GetNormalFromHeightMapForVertex(int32 heightMapIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("Getting and Setting Normal"));

	int32 prevX, prevY, nextX, nextY;
	GetXYIndexesAroundArrayIndex(heightMapIndex, &prevX, &prevY, &nextX, &nextY);

	// Get the 4 neighbouring points
	FVector up, down, left, right;
	GetDirectionVectorsAroundIndex(heightMapIndex, prevX, prevY, nextX, nextY, &up, &down, &left, &right);

	FVector vector = GetNormalsForEachSectionAroundIndex(up, down, left, right);

	return vector.GetSafeNormal();
}

FIntPoint ATile::GetXIndexYIndexFromIndex(int32 Index) {
	UE_LOG(LogTemp, Warning, TEXT("Index %d"), Index);
	int32 PlaneWidth = HalfWidth * 2;
	int32 XIndex = Index % PlaneWidth;
	int32 YIndex = Index / PlaneWidth;
	return FIntPoint(XIndex, YIndex);
}

void ATile::GenerateRandomHeightMap(int32 Index, FIntPoint indexPoint) {
	UE_LOG(LogTemp, Warning, TEXT("Getting Height"));
	float height = (NoiseGenerator->GetNoise2D((indexPoint.X - HalfWidth)*CellSize, (indexPoint.Y - HalfWidth)*CellSize) - 0.5) * CellSize * Height;
	UE_LOG(LogTemp, Warning, TEXT("Setting Height"));
	HeightMap.Emplace(Index, FVector((indexPoint.X - HalfWidth)*CellSize, (indexPoint.Y - HalfWidth)*CellSize, height));
}

void ATile::GenerateInitialize() {
	bGenerated = true;
	Vertices.SetNum(HalfWidth*HalfWidth * 4);
}

void ATile::GenerateNormalAndPositionForEachIndex() {
	for (int Index = 0; Index < (HalfWidth * 2)*(HalfWidth * 2); Index++)
	{
		FIntPoint indexPoint = GetXIndexYIndexFromIndex(Index);
		GenerateRandomHeightMap(Index, indexPoint);
		Vertices[Index].Normal = GetNormalFromHeightMapForVertex(Index);
		UE_LOG(LogTemp, Warning, TEXT("Setting Position"));
		Vertices[Index].Position = HeightMap[Index];
	}
}

void ATile::GenerateAllVertexAndTriangleData() {
	GenerateNormalAndPositionForEachIndex();

	UE_LOG(LogTemp, Warning, TEXT("Getting Triangles"));
	URuntimeMeshLibrary::CreateGridMeshTriangles(HalfWidth * 2, HalfWidth * 2, true, Triangles);
	UE_LOG(LogTemp, Warning, TEXT("Getting Tangents"));
	URuntimeMeshLibrary::CalculateTangentsForMesh(Vertices, Triangles);
}

void ATile::Generate()
{
	if (!bGenerated && isSetup)
	{
		UE_LOG(LogTemp, Warning, TEXT("Generating Tile"));
		
		GenerateInitialize();

		GenerateAllVertexAndTriangleData();

		UE_LOG(LogTemp, Warning, TEXT("Creating mesh"));
		RuntimeMesh->CreateMeshSection(0, Vertices, Triangles, true, EUpdateFrequency::Infrequent);
	}
}

void ATile::SetupNG(UUFNNoiseGenerator *ang) {
	NoiseGenerator = ang;
	isSetup = true;
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Generate();
}


