#include "AutomationTest.h"
#include "Tile.h"


IMPLEMENT_SIMPLE_AUTOMATION_TEST(FGetNormalFromHeightMapForVertexTest, "Duality.Tile.GetNormalFromHeightMapForVertex", EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter)
bool FGetNormalFromHeightMapForVertexTest::RunTest(const FString& Parameters)
{
	UE_LOG(LogTemp, Log, TEXT("Start Test"));
	ATile * tile = NewObject<ATile>();
	tile->SetupTile(2, 100.0f, 50.0f);
	TMap<int32, FVector> heightmap;
	heightmap.Emplace(0, FVector(-1.0f, -1.0f, 0.0f));
	heightmap.Emplace(1, FVector(0.0f, -1.0f, 0.0f));
	heightmap.Emplace(2, FVector(1.0f, -1.0f, 0.0f));
	heightmap.Emplace(3, FVector(2.0f, -1.0f, 0.0f));

	heightmap.Emplace(4, FVector(-1.0f, 0.0f, 0.0f));
	heightmap.Emplace(5, FVector(0.0f, 0.0f, 0.0f));
	heightmap.Emplace(6, FVector(1.0f, 0.0f, 0.0f));
	heightmap.Emplace(7, FVector(2.0f, 0.0f, 0.0f));

	heightmap.Emplace(8, FVector(-1.0f, 1.0f, 0.0f));
	heightmap.Emplace(9, FVector(0.0f, 1.0f, 0.0f));
	heightmap.Emplace(10, FVector(1.0f, 1.0f, 0.0f));
	heightmap.Emplace(11, FVector(2.0f, 1.0f, 0.0f));

	heightmap.Emplace(12, FVector(-1.0f, 2.0f, 0.0f));
	heightmap.Emplace(13, FVector(0.0f, 2.0f, 0.0f));
	heightmap.Emplace(14, FVector(1.0f, 2.0f, 0.0f));
	heightmap.Emplace(15, FVector(2.0f, 2.0f, 0.0f));

	tile->SetHeightMap(heightmap);

	for (int Index = 0; Index < 16; Index++) {
		FVector vector = tile->GetNormalFromHeightMapForVertex(Index);
		if (vector != FVector(0.0f, 0.0f, 1.0f)) {
			UE_LOG(LogTemp, Log, TEXT("(%s != FVector(0.0f, 0.0f, 1.0f)) at Index %d"), *(vector.ToString()), Index);
			delete tile;
			return false;
		}
	}

	heightmap.Emplace(0, FVector(-1.0f, -1.0f, 0.0f));
	heightmap.Emplace(1, FVector(0.0f, -1.0f, 0.0f));
	heightmap.Emplace(2, FVector(1.0f, -1.0f, 0.0f));
	heightmap.Emplace(3, FVector(2.0f, -1.0f, 0.0f));

	heightmap.Emplace(4, FVector(-1.0f, 0.0f, -1.0f));
	heightmap.Emplace(5, FVector(0.0f, 0.0f, -1.0f));
	heightmap.Emplace(6, FVector(1.0f, 0.0f, -1.0f));
	heightmap.Emplace(7, FVector(2.0f, 0.0f, -1.0f));

	heightmap.Emplace(8, FVector(-1.0f, 1.0f, -2.0f));
	heightmap.Emplace(9, FVector(0.0f, 1.0f, -2.0f));
	heightmap.Emplace(10, FVector(1.0f, 1.0f, -2.0f));
	heightmap.Emplace(11, FVector(2.0f, 1.0f, -2.0f));

	heightmap.Emplace(12, FVector(-1.0f, 2.0f, -3.0f));
	heightmap.Emplace(13, FVector(0.0f, 2.0f, -3.0f));
	heightmap.Emplace(14, FVector(1.0f, 2.0f, -3.0f));
	heightmap.Emplace(15, FVector(2.0f, 2.0f, -3.0f));

	tile->SetHeightMap(heightmap);

	FVector vector = tile->GetNormalFromHeightMapForVertex(4);
	if (vector != FVector(0.0f, FGenericPlatformMath::Sqrt(0.5f), FGenericPlatformMath::Sqrt(0.5f))) {
		UE_LOG(LogTemp, Log, TEXT("(%s != FVector(0.0f, 0.707f, 0.707f))"), *(vector.ToString()));
		delete tile;
		return false;
	}

	delete tile;
	return true;
}

IMPLEMENT_SIMPLE_AUTOMATION_TEST(FGetXIndexYIndexFromIndexTest, "Duality.Tile.GetXIndexYIndexFromIndex", EAutomationTestFlags::ApplicationContextMask | EAutomationTestFlags::ProductFilter)
bool FGetXIndexYIndexFromIndexTest::RunTest(const FString& Parameters)
{
	UE_LOG(LogTemp, Log, TEXT("Start Test"));
	ATile * tile = NewObject<ATile>();
	tile->SetupTile(20, 100.0f, 50.0f);

	FIntPoint index0 = tile->GetXIndexYIndexFromIndex(0);
	if (!index0.X == 0 || !index0.Y == 0) {
		UE_LOG(LogTemp, Log, TEXT("(!index0.X == 0 || !index0.Y == 0)"));
		delete tile;
		return false;
	}

	FIntPoint index1 = tile->GetXIndexYIndexFromIndex(40);
	if (!index1.X == 0 || !index1.Y == 1) {
		UE_LOG(LogTemp, Log, TEXT("(!index0.X == 0 || !index0.Y == 1)"));
		delete tile;
		return false;
	}

	FIntPoint index2 = tile->GetXIndexYIndexFromIndex(1);
	if (!index2.X == 1 || !index2.Y == 0) {
		UE_LOG(LogTemp, Log, TEXT("(!index0.X == 0 || !index0.Y == 1)"));
		delete tile;
		return false;
	}

	delete tile;
	return true;
}