// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RuntimeMeshComponent.h"
#include "UFNNoiseGenerator.h"
#include "Tile.generated.h"

UCLASS(BlueprintType, Blueprintable)
class DUALITY_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	ATile();

private:
	int32 HalfWidth = 20;
	float CellSize = 100.0f;
	float Height = 50.0f;
	TMap<int32, FVector> HeightMap;
	TArray<FRuntimeMeshVertexSimple> Vertices;
	TArray<int32> Triangles;
	bool bGenerated;
	bool isSetup;

	void GetXYIndexesAroundArrayIndex(int32 Index, int32 *prevX, int32 *prevY, int32 *nextX, int32 *nextY);
	void GetDirectionVectorsAroundIndex(int32 Index, int32 prevX, int32 prevY, int32 nextX, int32 nextY, FVector *up, FVector *down, FVector *left, FVector *right);
	FVector GetNormalsForEachSectionAroundIndex(FVector up, FVector down, FVector left, FVector right);

	void GenerateRandomHeightMap(int32 Index, FIntPoint indexPoint);
	void GenerateInitialize();
	void GenerateNormalAndPositionForEachIndex();
	void GenerateAllVertexAndTriangleData();

public:	
	void SetHeightMap(TMap<int32, FVector> heightmap);
	FVector GetNormalFromHeightMapForVertex(int32 heightMapIndex);
	FIntPoint GetXIndexYIndexFromIndex(int32 Index);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY()
	URuntimeMeshComponent* RuntimeMesh;
	
	UPROPERTY()
	UUFNNoiseGenerator *NoiseGenerator;

	void Generate();

	UFUNCTION(BlueprintCallable, Category = "AAAA")
	void SetupNG(UUFNNoiseGenerator *ang);

	UFUNCTION(BlueprintCallable, Category = "AAAA")
	void SetupTile(int32 HalfWidth, float CellSize, float Height);
};
