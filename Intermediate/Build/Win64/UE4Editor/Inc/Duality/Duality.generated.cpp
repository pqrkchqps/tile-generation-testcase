// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Duality.generated.dep.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode1Duality() {}
#if USE_COMPILED_IN_NATIVES
// Cross Module References
	ENGINE_API class UClass* Z_Construct_UClass_AGameModeBase();
	UNREALFASTNOISEPLUGIN_API class UClass* Z_Construct_UClass_UUFNNoiseGenerator_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_AActor();
	RUNTIMEMESHCOMPONENT_API class UClass* Z_Construct_UClass_URuntimeMeshComponent_NoRegister();

	DUALITY_API class UClass* Z_Construct_UClass_ADualityGameModeBase_NoRegister();
	DUALITY_API class UClass* Z_Construct_UClass_ADualityGameModeBase();
	DUALITY_API class UFunction* Z_Construct_UFunction_ATile_SetupNG();
	DUALITY_API class UFunction* Z_Construct_UFunction_ATile_SetupTile();
	DUALITY_API class UClass* Z_Construct_UClass_ATile_NoRegister();
	DUALITY_API class UClass* Z_Construct_UClass_ATile();
	DUALITY_API class UPackage* Z_Construct_UPackage__Script_Duality();
	void ADualityGameModeBase::StaticRegisterNativesADualityGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_ADualityGameModeBase_NoRegister()
	{
		return ADualityGameModeBase::StaticClass();
	}
	UClass* Z_Construct_UClass_ADualityGameModeBase()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AGameModeBase();
			Z_Construct_UPackage__Script_Duality();
			OuterClass = ADualityGameModeBase::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900288;


				static TCppClassTypeInfo<TCppClassTypeTraits<ADualityGameModeBase> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("DualityGameModeBase.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("DualityGameModeBase.h"));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADualityGameModeBase, 1073263667);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADualityGameModeBase(Z_Construct_UClass_ADualityGameModeBase, &ADualityGameModeBase::StaticClass, TEXT("/Script/Duality"), TEXT("ADualityGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADualityGameModeBase);
	void ATile::StaticRegisterNativesATile()
	{
		UClass* Class = ATile::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "SetupNG", (Native)&ATile::execSetupNG },
			{ "SetupTile", (Native)&ATile::execSetupTile },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 2);
	}
	UFunction* Z_Construct_UFunction_ATile_SetupNG()
	{
		struct Tile_eventSetupNG_Parms
		{
			UUFNNoiseGenerator* ang;
		};
		UObject* Outer=Z_Construct_UClass_ATile();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SetupNG"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(Tile_eventSetupNG_Parms));
			UProperty* NewProp_ang = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ang"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ang, Tile_eventSetupNG_Parms), 0x0010000000000080, Z_Construct_UClass_UUFNNoiseGenerator_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("AAAA"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Tile.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ATile_SetupTile()
	{
		struct Tile_eventSetupTile_Parms
		{
			int32 HalfWidth;
			float CellSize;
			float Height;
		};
		UObject* Outer=Z_Construct_UClass_ATile();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SetupTile"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(Tile_eventSetupTile_Parms));
			UProperty* NewProp_Height = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Height"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(Height, Tile_eventSetupTile_Parms), 0x0010000000000080);
			UProperty* NewProp_CellSize = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("CellSize"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(CellSize, Tile_eventSetupTile_Parms), 0x0010000000000080);
			UProperty* NewProp_HalfWidth = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("HalfWidth"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(HalfWidth, Tile_eventSetupTile_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("AAAA"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Tile.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ATile_NoRegister()
	{
		return ATile::StaticClass();
	}
	UClass* Z_Construct_UClass_ATile()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage__Script_Duality();
			OuterClass = ATile::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;

				OuterClass->LinkChild(Z_Construct_UFunction_ATile_SetupNG());
				OuterClass->LinkChild(Z_Construct_UFunction_ATile_SetupTile());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_NoiseGenerator = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("NoiseGenerator"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(NoiseGenerator, ATile), 0x0010000000000000, Z_Construct_UClass_UUFNNoiseGenerator_NoRegister());
				UProperty* NewProp_RuntimeMesh = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("RuntimeMesh"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(RuntimeMesh, ATile), 0x0010000000080008, Z_Construct_UClass_URuntimeMeshComponent_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ATile_SetupNG(), "SetupNG"); // 2497272346
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ATile_SetupTile(), "SetupTile"); // 3418435614
				static TCppClassTypeInfo<TCppClassTypeTraits<ATile> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintType"), TEXT("true"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Tile.h"));
				MetaData->SetValue(OuterClass, TEXT("IsBlueprintBase"), TEXT("true"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Tile.h"));
				MetaData->SetValue(NewProp_NoiseGenerator, TEXT("ModuleRelativePath"), TEXT("Tile.h"));
				MetaData->SetValue(NewProp_RuntimeMesh, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_RuntimeMesh, TEXT("ModuleRelativePath"), TEXT("Tile.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATile, 932412129);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATile(Z_Construct_UClass_ATile, &ATile::StaticClass, TEXT("/Script/Duality"), TEXT("ATile"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATile);
	UPackage* Z_Construct_UPackage__Script_Duality()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			ReturnPackage = CastChecked<UPackage>(StaticFindObjectFast(UPackage::StaticClass(), nullptr, FName(TEXT("/Script/Duality")), false, false));
			ReturnPackage->SetPackageFlags(PKG_CompiledIn | 0x00000000);
			FGuid Guid;
			Guid.A = 0xFCC9C2FF;
			Guid.B = 0xA09C04C6;
			Guid.C = 0x00000000;
			Guid.D = 0x00000000;
			ReturnPackage->SetGuid(Guid);

		}
		return ReturnPackage;
	}
#endif
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
