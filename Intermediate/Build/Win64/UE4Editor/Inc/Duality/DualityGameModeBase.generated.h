// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DUALITY_DualityGameModeBase_generated_h
#error "DualityGameModeBase.generated.h already included, missing '#pragma once' in DualityGameModeBase.h"
#endif
#define DUALITY_DualityGameModeBase_generated_h

#define Duality_Source_Duality_DualityGameModeBase_h_15_RPC_WRAPPERS
#define Duality_Source_Duality_DualityGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Duality_Source_Duality_DualityGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADualityGameModeBase(); \
	friend DUALITY_API class UClass* Z_Construct_UClass_ADualityGameModeBase(); \
public: \
	DECLARE_CLASS(ADualityGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/Duality"), NO_API) \
	DECLARE_SERIALIZER(ADualityGameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Duality_Source_Duality_DualityGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesADualityGameModeBase(); \
	friend DUALITY_API class UClass* Z_Construct_UClass_ADualityGameModeBase(); \
public: \
	DECLARE_CLASS(ADualityGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/Duality"), NO_API) \
	DECLARE_SERIALIZER(ADualityGameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Duality_Source_Duality_DualityGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADualityGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADualityGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADualityGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADualityGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADualityGameModeBase(ADualityGameModeBase&&); \
	NO_API ADualityGameModeBase(const ADualityGameModeBase&); \
public:


#define Duality_Source_Duality_DualityGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADualityGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADualityGameModeBase(ADualityGameModeBase&&); \
	NO_API ADualityGameModeBase(const ADualityGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADualityGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADualityGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADualityGameModeBase)


#define Duality_Source_Duality_DualityGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Duality_Source_Duality_DualityGameModeBase_h_12_PROLOG
#define Duality_Source_Duality_DualityGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Duality_Source_Duality_DualityGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Duality_Source_Duality_DualityGameModeBase_h_15_RPC_WRAPPERS \
	Duality_Source_Duality_DualityGameModeBase_h_15_INCLASS \
	Duality_Source_Duality_DualityGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Duality_Source_Duality_DualityGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Duality_Source_Duality_DualityGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Duality_Source_Duality_DualityGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Duality_Source_Duality_DualityGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Duality_Source_Duality_DualityGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Duality_Source_Duality_DualityGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
