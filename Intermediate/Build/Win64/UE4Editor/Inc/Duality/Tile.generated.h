// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UUFNNoiseGenerator;
#ifdef DUALITY_Tile_generated_h
#error "Tile.generated.h already included, missing '#pragma once' in Tile.h"
#endif
#define DUALITY_Tile_generated_h

#define Duality_Source_Duality_Tile_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetupTile) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_HalfWidth); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_CellSize); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Height); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SetupTile(Z_Param_HalfWidth,Z_Param_CellSize,Z_Param_Height); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetupNG) \
	{ \
		P_GET_OBJECT(UUFNNoiseGenerator,Z_Param_ang); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SetupNG(Z_Param_ang); \
		P_NATIVE_END; \
	}


#define Duality_Source_Duality_Tile_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetupTile) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_HalfWidth); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_CellSize); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Height); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SetupTile(Z_Param_HalfWidth,Z_Param_CellSize,Z_Param_Height); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetupNG) \
	{ \
		P_GET_OBJECT(UUFNNoiseGenerator,Z_Param_ang); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SetupNG(Z_Param_ang); \
		P_NATIVE_END; \
	}


#define Duality_Source_Duality_Tile_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATile(); \
	friend DUALITY_API class UClass* Z_Construct_UClass_ATile(); \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Duality"), NO_API) \
	DECLARE_SERIALIZER(ATile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Duality_Source_Duality_Tile_h_14_INCLASS \
private: \
	static void StaticRegisterNativesATile(); \
	friend DUALITY_API class UClass* Z_Construct_UClass_ATile(); \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Duality"), NO_API) \
	DECLARE_SERIALIZER(ATile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Duality_Source_Duality_Tile_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public:


#define Duality_Source_Duality_Tile_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATile)


#define Duality_Source_Duality_Tile_h_14_PRIVATE_PROPERTY_OFFSET
#define Duality_Source_Duality_Tile_h_11_PROLOG
#define Duality_Source_Duality_Tile_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Duality_Source_Duality_Tile_h_14_PRIVATE_PROPERTY_OFFSET \
	Duality_Source_Duality_Tile_h_14_RPC_WRAPPERS \
	Duality_Source_Duality_Tile_h_14_INCLASS \
	Duality_Source_Duality_Tile_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Duality_Source_Duality_Tile_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Duality_Source_Duality_Tile_h_14_PRIVATE_PROPERTY_OFFSET \
	Duality_Source_Duality_Tile_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Duality_Source_Duality_Tile_h_14_INCLASS_NO_PURE_DECLS \
	Duality_Source_Duality_Tile_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Duality_Source_Duality_Tile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
