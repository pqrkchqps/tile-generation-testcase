// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OCEANPLUGIN_BuoyancyForceComponent_generated_h
#error "BuoyancyForceComponent.generated.h already included, missing '#pragma once' in BuoyancyForceComponent.h"
#endif
#define OCEANPLUGIN_BuoyancyForceComponent_generated_h

#define Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_31_GENERATED_BODY \
	friend OCEANPLUGIN_API class UScriptStruct* Z_Construct_UScriptStruct_FStructBoneOverride(); \
	static class UScriptStruct* StaticStruct();


#define Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_57_RPC_WRAPPERS
#define Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_57_RPC_WRAPPERS_NO_PURE_DECLS
#define Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_57_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBuoyancyForceComponent(); \
	friend OCEANPLUGIN_API class UClass* Z_Construct_UClass_UBuoyancyForceComponent(); \
public: \
	DECLARE_CLASS(UBuoyancyForceComponent, USceneComponent, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/OceanPlugin"), OCEANPLUGIN_API) \
	DECLARE_SERIALIZER(UBuoyancyForceComponent) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_57_INCLASS \
private: \
	static void StaticRegisterNativesUBuoyancyForceComponent(); \
	friend OCEANPLUGIN_API class UClass* Z_Construct_UClass_UBuoyancyForceComponent(); \
public: \
	DECLARE_CLASS(UBuoyancyForceComponent, USceneComponent, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/OceanPlugin"), OCEANPLUGIN_API) \
	DECLARE_SERIALIZER(UBuoyancyForceComponent) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_57_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	OCEANPLUGIN_API UBuoyancyForceComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBuoyancyForceComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OCEANPLUGIN_API, UBuoyancyForceComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBuoyancyForceComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OCEANPLUGIN_API UBuoyancyForceComponent(UBuoyancyForceComponent&&); \
	OCEANPLUGIN_API UBuoyancyForceComponent(const UBuoyancyForceComponent&); \
public:


#define Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_57_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	OCEANPLUGIN_API UBuoyancyForceComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OCEANPLUGIN_API UBuoyancyForceComponent(UBuoyancyForceComponent&&); \
	OCEANPLUGIN_API UBuoyancyForceComponent(const UBuoyancyForceComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OCEANPLUGIN_API, UBuoyancyForceComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBuoyancyForceComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBuoyancyForceComponent)


#define Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_57_PRIVATE_PROPERTY_OFFSET
#define Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_54_PROLOG
#define Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_57_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_57_PRIVATE_PROPERTY_OFFSET \
	Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_57_RPC_WRAPPERS \
	Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_57_INCLASS \
	Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_57_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_57_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_57_PRIVATE_PROPERTY_OFFSET \
	Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_57_RPC_WRAPPERS_NO_PURE_DECLS \
	Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_57_INCLASS_NO_PURE_DECLS \
	Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h_57_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class BuoyancyForceComponent."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Duality_Plugins_OceanPlugin_Source_OceanPlugin_Classes_BuoyancyForceComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
